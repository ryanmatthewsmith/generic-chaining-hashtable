package datastructures.dictionaries;

import java.util.Iterator;
import java.util.function.Supplier;

import cse332.datastructures.containers.*;
import cse332.interfaces.misc.DeletelessDictionary;
import cse332.interfaces.misc.Dictionary;
import cse332.interfaces.misc.SimpleIterator;
//import datastructures.dictionaries.MoveToFrontList.Node;

/**
 *  A subclass of DeletelessDictionary, this dictionary is implemented
 *  as a hash table with separate chaining used for collision resolution.
 *  Built in limitations allow size of table only up to 835001 Key-Value 
 *  pairs.  HashTable will resize and rehash when load factor is reached.
 *  Table will be an array, the chains for collision resolution will be
 *  a dictionary passed as an argument to constructor
 * 
 *  @author Ryan Matthew Smith
 *  @param <K> key   of key-value pair
 *  @param <V> value of key-value pair
 *  @param <Supplier<Dictionary<K,V> 
 *      Dictionary supplied for separate chaining
 */
public class ChainingHashTable<K, V> extends DeletelessDictionary<K, V> {
    private final int[] PRIMES = {11,23,47,97,199,401,809,1619,3251,
                                  6521,13043,26099,52201,104417,
                                  208799,417553,835001,1700053};
    private final static double LOADMAX = 1.0;
    private int currentPrimeIndex;

    private Dictionary<K, V>[] table;
    
    private final Supplier<Dictionary<K, V>> newChain;  

    @SuppressWarnings("unchecked")
    public ChainingHashTable(Supplier<Dictionary<K, V>> newChain) {
        this.newChain = newChain;

        table = (Dictionary<K,V>[])new Dictionary[PRIMES[0]];
        for(int i = 0; i<PRIMES[0]; i++){
            table[i]= newChain.get();
        }
        currentPrimeIndex = 0;
        this.size = 0;
    }

    @Override
    public V insert(K key, V value) {
        
        //add loadfactor check here, if reload increment prime index and reload
        if((double)this.size/(double)table.length > LOADMAX){
            resizeTable();
        }
        
        int tableIndex = key.hashCode()%table.length;
        
        //insert the key, value: if key existed, return old val, else inc size, return null
        V valOld = table[Math.abs(tableIndex)].insert(key,value);
        if (valOld != null){
            //size same and must return value
            return valOld;
        }
        size++;
        return null;
    }

    @Override
    public V find(K key) {

        int tableIndex = key.hashCode()%table.length;
        
        V val = table[Math.abs(tableIndex)].find(key);
        
        if(val == null){
            return null;
        }
        return val;
    }
    @SuppressWarnings("unchecked")
    private void resizeTable(){
        currentPrimeIndex++;
        long tableSize;
        //set up
        if (currentPrimeIndex < table.length){
          tableSize = PRIMES[currentPrimeIndex];
        } else{
          tableSize = table.length*2;
            
        }
        Dictionary<K,V>[] tableNew = (Dictionary<K,V>[])new Dictionary[PRIMES[currentPrimeIndex]];
        for(int i=0; i<tableNew.length;i++){
           tableNew[i] = newChain.get();
        }
        Iterator<Item<K,V>> iter = iterator();
        
        //grab each Item, rehash, place in tableNew
        while(iter.hasNext()){
            Item<K,V> curr = iter.next();
            int tableIndex = curr.key.hashCode()%tableNew.length;
                        
            tableNew[Math.abs(tableIndex)].insert(curr.key, curr.value);           
        }
        table = tableNew;
    }

    @Override
    public Iterator<Item<K, V>> iterator() {
        return new MyIterator(this.size);
    }
    
    public class MyIterator extends SimpleIterator<Item<K, V>>{
        private final Item<K, V>[] iter;
        private final int sizeArray;
        private int iterIdx;
        
        @SuppressWarnings("unchecked")
        public MyIterator(int size){
            sizeArray = size;
            iterIdx = 0;
            iter = (Item<K,V>[])new Item[sizeArray];

            int index = 0;
            for(int i=0;i<table.length;i++){
                for(Item<K,V> item : table[i]){
                    iter[index]=item;
                    index++;
                }
            }
        }
        public boolean hasNext(){
            return(iterIdx < sizeArray);
        }
        
        public Item<K, V> next(){
            if(!hasNext()){
                return null;
            }
            Item<K,V> item = iter[iterIdx];
            iterIdx++;
            return item;
        }
    }
    
}
